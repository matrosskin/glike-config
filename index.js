var _ = require('lodash');

global.config = global.config || {};

var appRunPath = process.cwd();

var arguments = {};
var argument = null;
for (var i=2; i<process.argv.length; i++) {
	if ( argument = process.argv[i].match(/^--(\w+)=(\w+)$/) ) {
		arguments[argument[1]] = argument[2];
	}
}

var configPath = arguments.conf || (appRunPath + '/config/config');

var configJSON = require(configPath);

var environments = configJSON.environments;
configJSON.environments = null;
_.extend(global.config, configJSON);

var environment = arguments.env || 'dev';
global.config.environment = environment;
if (environments[environment]) {
	_.extend(global.config, environments[environment]);
}

module.exports = global.config;
